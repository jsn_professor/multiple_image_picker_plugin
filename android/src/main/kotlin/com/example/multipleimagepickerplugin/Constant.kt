package com.example.multipleimagepickerplugin

/**
 * Created by Professor on 12/11/15.
 */
class Constant {
    object Key {
        const val IMAGES = "IMAGES"
    }

    object Extra {
        const val MAX_IMAGE_COUNT = "MAX_SIZE"
    }

    object RequestCode {
        const val IMAGES = 0
    }
}
