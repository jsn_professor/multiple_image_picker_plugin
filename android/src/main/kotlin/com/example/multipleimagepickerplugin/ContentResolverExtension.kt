package com.example.multipleimagepickerplugin

import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import java.io.File

fun ContentResolver.getFileFromUri(context: Context, uri: Uri?): File? {
    if (uri == null) return null
    if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context, uri))
        when (uri.authority) {
            "com.android.externalstorage.documents" -> {
                val split = DocumentsContract.getDocumentId(uri).split(":")
                return if ("primary" == split.firstOrNull()) {
                    File("${Environment.getExternalStorageDirectory().absolutePath}/${split.lastOrNull()}")
                } else {
                    File("/storage/${split.firstOrNull()}/${split.lastOrNull()}")
                }
            }
            "com.android.providers.downloads.documents" -> {
                val id = DocumentsContract.getDocumentId(uri)
                if (id.startsWith("raw:")) return File(id.replaceFirst("raw:".toRegex(), ""))
                val downloadUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), id.toLong())
                val path = queryAbsolutePath(downloadUri)
                return File(path)
            }
            "com.android.providers.media.documents" -> {
                val split = DocumentsContract.getDocumentId(uri).split(":")
                val mediaUri = ContentUris.withAppendedId(when (split.firstOrNull()) {
                    "image" -> MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    else -> return null
                }, java.lang.Long.parseLong(split[1]))
                val path = queryAbsolutePath(mediaUri)
                return File(path)
            }
        }
    else {
        var path: String? = null
        when (uri.scheme) {
            "content" -> path = queryAbsolutePath(uri)
            "file" -> path = uri.path
        }
        return File(path)
    }
    return null
}

fun ContentResolver.queryAbsolutePath(uri: Uri?): String? {
    uri?.let {
        query(it, arrayOf(MediaStore.MediaColumns.DATA), null, null, null)?.let {
            it.moveToFirst()
            val data = it.getString(it.getColumnIndex(MediaStore.MediaColumns.DATA))
            it.close()
            return data
        }
    }
    return null
}