package com.example.multipleimagepickerplugin

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_image_picker.*

class MultipleImagePickerActivity : Activity() {
    private lateinit var requestBuilder: RequestBuilder<Drawable>
    private var maxImageCount = 4
    private var menuNext: MenuItem? = null
    private val images = mutableListOf<RecyclerViewItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_picker)
        actionBar?.setDisplayShowTitleEnabled(false)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_close)
        requestBuilder = Glide.with(this).asDrawable().transition(DrawableTransitionOptions.withCrossFade())
        maxImageCount = intent.getIntExtra(Constant.Extra.MAX_IMAGE_COUNT, 4)
        contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            arrayOf(MediaStore.Images.Media._ID),
            null,
            null,
            null
        )?.let {
            while (it.moveToNext()) {
                images.add(RecyclerViewItem(Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, it.getLong(it.getColumnIndex(MediaStore.MediaColumns._ID)).toString())))
            }
            it.close()
        }
        recyclerView.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 3)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = ImagePickerAdapter()
        if (images.isEmpty()) {
            textViewEmpty.visibility = View.VISIBLE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_image_picker, menu)
        menuNext = menu?.findItem(R.id.menu_next)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.menu_next -> {
                setResult(Activity.RESULT_OK, Intent().apply {
                    putExtra(Constant.Key.IMAGES, images.filter { it.isSelected }.map { it.image }.toTypedArray())
                })
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private inner class ImagePickerAdapter : RecyclerView.Adapter<ImagePickerAdapter.ViewHolder>() {
        override fun onCreateViewHolder(group: ViewGroup, viewType: Int): ImagePickerAdapter.ViewHolder {
            return ViewHolder(layoutInflater.inflate(R.layout.cell_image_picker_activity, group, false))
        }

        override fun getItemCount(): Int {
            return images.size
        }

        override fun onBindViewHolder(holder: ImagePickerAdapter.ViewHolder, position: Int) {
            val item = images[position]
            requestBuilder.load(item.image).listener(holder).into(holder.imageView)
            holder.count.text = "${item.number}"
            if (item.isSelected) {
                holder.mask.alpha = 1f
            } else {
                holder.mask.alpha = 0f
            }
        }

        private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener, RequestListener<Drawable> {
            val imageView: ImageView = itemView.findViewById(R.id.imageViewThumbnailCell)
            val mask: FrameLayout = itemView.findViewById(R.id.frameLayoutMaskCell)
            val count: TextView = itemView.findViewById(R.id.textViewCountCell)
            val progressBar: ProgressBar = itemView.findViewById(R.id.progressBarCell)

            init {
                itemView.setOnClickListener(this)
            }

            override fun onClick(v: View?) {
                if (recyclerView.isAnimating) {
                    return
                }
                val selectedCount = images.count { it.isSelected }
                val image = images[adapterPosition]
                val isSelected = !image.isSelected
                if (isSelected && selectedCount >= maxImageCount) {
                    return
                }
                image.isSelected = isSelected
                if (isSelected) {
                    menuNext?.isEnabled = true
                    image.number = selectedCount + 1
                    count.text = "${image.number}"
                    ObjectAnimator.ofFloat(mask, View.ALPHA, 0f, 1f).start()
                } else {
                    if (selectedCount <= 1) {
                        menuNext?.isEnabled = false
                    }
                    images.forEachIndexed { index, recyclerViewItem ->
                        if (recyclerViewItem.isSelected && recyclerViewItem.number > image.number) {
                            recyclerViewItem.number--
                            notifyItemChanged(index)
                        }
                    }
                    ObjectAnimator.ofFloat(mask, View.ALPHA, 1f, 0f).start()
                }
            }

            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                progressBar.visibility = View.INVISIBLE
                return false
            }

            override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: com.bumptech.glide.load.DataSource, isFirstResource: Boolean): Boolean {
                progressBar.visibility = View.INVISIBLE
                return false
            }
        }
    }

    private inner class RecyclerViewItem(var image: Uri) {
        var isSelected = false
        var number = 0
    }
}