package com.example.multipleimagepickerplugin

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry

/** MultipleImagePickerPlugin */
class MultipleImagePickerPlugin : FlutterPlugin, ActivityAware, PluginRegistry.ActivityResultListener, MethodCallHandler {
    private var binding: ActivityPluginBinding? = null
    private var methodChannel: MethodChannel? = null
    private var pendingResult: Result? = null

    override fun onAttachedToEngine(@NonNull fluginBinding: FlutterPlugin.FlutterPluginBinding) {
        onAttachedToEngine(fluginBinding.applicationContext, fluginBinding.binaryMessenger)
    }

    private fun onAttachedToEngine(context: Context?, messenger: BinaryMessenger?) {
        methodChannel = MethodChannel(messenger, "multiple_image_picker_plugin_method_channel").apply {
            setMethodCallHandler(this@MultipleImagePickerPlugin)
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        methodChannel?.setMethodCallHandler(null)
        methodChannel = null
        pendingResult = null
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "pickImages" -> {
                val activity = binding?.activity ?: return result.success(listOf<String>())
                pendingResult?.success(null)
                pendingResult = result
                activity.startActivityForResult(
                    Intent(activity, MultipleImagePickerActivity::class.java).apply {
                        putExtra(Constant.Extra.MAX_IMAGE_COUNT, call.arguments as? Int)
                    }, Constant.RequestCode.IMAGES
                )
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        binding.addActivityResultListener(this)
        this.binding = binding
    }

    override fun onDetachedFromActivity() {
        binding?.removeActivityResultListener(this)
        binding = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        onAttachedToActivity(binding)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        onDetachedFromActivity()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        val activity = binding?.activity
        return if (requestCode == Constant.RequestCode.IMAGES) {
            if (resultCode == Activity.RESULT_OK && activity != null) {
                val uris = data?.getParcelableArrayExtra(Constant.Key.IMAGES)?.map { (it as Uri) }
                val files = uris?.map { activity.contentResolver?.getFileFromUri(activity, it) }
                val result = files?.map { it?.absolutePath }
                pendingResult?.success(result)
            } else {
                pendingResult?.success(null)
            }
            pendingResult = null
            true
        } else {
            false
        }
    }
}
