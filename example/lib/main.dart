import "dart:io";

import "package:flutter/material.dart";
import "package:multiple_image_picker_plugin/multiple_image_picker_plugin.dart";
import 'package:permission_handler/permission_handler.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List _images = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Plugin example app"),
        ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: _images.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) => Center(child: Image.file(File(_images[index]))),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  child: Text("Pick images"),
                  onPressed: () async {
                    var status = Platform.isAndroid ? await Permission.storage.status : await Permission.photos.status;
                    var shouldShowRequestRationale = Platform.isAndroid ? await Permission.storage.shouldShowRequestRationale : false;
                    var result = status;
                    if (result.isDenied) {
                      result = Platform.isAndroid ? await Permission.storage.request() : await Permission.photos.request();
                    }
                    switch (result) {
                      case PermissionStatus.granted:
                        var images = await MultiImagePickerPlugin.pickImages(8);
                        if (images != null) {
                          setState(() {
                            _images = images;
                          });
                        }
                        break;
                      case PermissionStatus.permanentlyDenied:
                        if (Platform.isAndroid && !shouldShowRequestRationale || status != PermissionStatus.denied) {
                          openAppSettings();
                        }
                        break;
                      default:
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
