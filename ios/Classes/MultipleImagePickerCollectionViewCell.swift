//
//  MultipleImagePickerCollectionViewCell.swift
//  multiple_image_picker_plugin
//
//  Created by Professor on 2020/6/16.
//

import UIKit

class MultipleImagePickerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var selectionMask: UIView!
    @IBOutlet weak var count: UILabel!
    var localIdentifier: String?
}
