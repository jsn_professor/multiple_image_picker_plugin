//
//  MultipleImagePickerViewController.swift
//  multiple_image_picker_plugin
//
//  Created by Professor on 2020/6/16.
//

import UIKit
import Photos
import iOSUtility

private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map {
            $0.indexPath
        }
    }
}

class MultipleImagePickerViewController: UIViewController, PHPhotoLibraryChangeObserver, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var labelEmpty: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    private let COLUMN_COUNT: CGFloat = 3
    private let manager = PHCachingImageManager()
    private var result: PHFetchResult<PHAsset>?
    private var selectedAssets = [PHAsset]()
    private var targetSize = CGSize.zero
    private var previousPreheatRect = CGRect.zero
    weak var delegate: ImagePickerViewControllerDelegate?
    var maxImageCount = 4

    override func viewDidLoad() {
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        result = PHAsset.fetchAssets(with: .image, options: options)
        labelEmpty.isHidden = result?.count != 0
        let size = floor(view.bounds.width / COLUMN_COUNT) * UIScreen.main.scale
        targetSize = CGSize(width: size, height: size)
        collectionView.allowsMultipleSelection = true
        PHPhotoLibrary.shared().register(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        updateCachedAssets()
    }

    func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async {
            if let result = self.result, let details = changeInstance.changeDetails(for: result) {
                self.result = details.fetchResultAfterChanges
                self.labelEmpty.isHidden = self.result?.count != 0
                if details.hasIncrementalChanges {
                    self.collectionView.performBatchUpdates({
                        if let removedIndexes = details.removedIndexes {
                            var indexPaths = [IndexPath]()
                            for index in removedIndexes {
                                indexPaths.append(IndexPath(row: index, section: 0))
                            }
                            self.collectionView.deleteItems(at: indexPaths)
                        }
                        if let insertedIndexes = details.insertedIndexes {
                            var indexPaths = [IndexPath]()
                            for index in insertedIndexes {
                                indexPaths.append(IndexPath(row: index, section: 0))
                            }
                            self.collectionView.insertItems(at: indexPaths)
                        }
                    }) {
                        finished in
                        if finished, let changeIndexes = details.changedIndexes {
                            var indexPaths = [IndexPath]()
                            for index in changeIndexes {
                                indexPaths.append(IndexPath(row: index, section: 0))
                            }
                            self.collectionView.reloadItems(at: indexPaths)
                        }
                    }
                } else {
                    self.collectionView.reloadSections([0])
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return result?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultipleImagePickerCollectionViewCell", for: indexPath) as! MultipleImagePickerCollectionViewCell
        if let asset = result?.object(at: indexPath.row) {
            if let index = selectedAssets.firstIndex(of: asset) {
                cell.count.text = "\(index + 1)"
                cell.selectionMask.isHidden = false
                cell.selectionMask.alpha = 1
            } else {
                cell.selectionMask.isHidden = true
                cell.selectionMask.alpha = 0
            }
            cell.localIdentifier = asset.localIdentifier
            manager.requestImage(
                    for: asset,
                    targetSize: targetSize,
                    contentMode: .aspectFill,
                    options: nil) {
                image, info in
                if cell.localIdentifier == asset.localIdentifier {
                    cell.imageView.image = image
                }
            }
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CollectionReusableView", for: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            let horizontalSpacing = layout.sectionInset.left + layout.sectionInset.right + (COLUMN_COUNT - 1) * layout.minimumInteritemSpacing
            let size = floor((collectionView.bounds.width - horizontalSpacing) / COLUMN_COUNT)
            return CGSize(width: size, height: size)
        }
        return .zero
    }

    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return selectedAssets.count < maxImageCount
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MultipleImagePickerCollectionViewCell {
            cell.count.text = "\(selectedAssets.count + 1)"
            cell.selectionMask.isHidden = false
            UIView.animate(withDuration: 0.3) {
                cell.selectionMask.alpha = 1
            }
        }
        navigationItem.rightBarButtonItem?.isEnabled = true
        guard let asset = result?.object(at: indexPath.row) else {
            return
        }
        selectedAssets.append(asset)
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MultipleImagePickerCollectionViewCell {
            UIView.animate(withDuration: 0.3, animations: {
                cell.selectionMask.alpha = 0
            }) {
                finished in
                cell.selectionMask.isHidden = true
            }
        }
        guard let result = result,
              let index = selectedAssets.firstIndex(of: result.object(at: indexPath.row)) else {
            return
        }
        selectedAssets.remove(at: index)
        navigationItem.rightBarButtonItem?.isEnabled = !selectedAssets.isEmpty
        let visibleIndexPaths = collectionView.indexPathsForVisibleItems
        let updateAssets = selectedAssets[index..<selectedAssets.count]
        var updateIndexPaths = [IndexPath]()
        for index in 0..<result.count {
            let updateIndexPath = IndexPath(row: index, section: 0)
            if visibleIndexPaths.contains(updateIndexPath) && updateAssets.contains(result.object(at: index)) {
                updateIndexPaths.append(updateIndexPath)
            }
        }
        for indexPath in updateIndexPaths {
            if let cell = collectionView.cellForItem(at: indexPath) as? MultipleImagePickerCollectionViewCell {
                let asset = result.object(at: indexPath.row)
                if let index = updateAssets.firstIndex(of: asset) {
                    cell.count.setText(text: "\(index + 1)")
                }
            }
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCachedAssets()
    }

    private func resetCachedAssets() {
        manager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }

    private func updateCachedAssets() {
        // Update only if the view is visible.
        guard let result = result, result.count > 0, isViewLoaded && view.window != nil else {
            return
        }

        // The preheat window is twice the height of the visible rect.
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let preheatRect = visibleRect.insetBy(dx: 0, dy: -0.5 * visibleRect.height)

        // Update only if the visible area is significantly different from the last preheated area.
        let delta = abs(preheatRect.midY - previousPreheatRect.midY)
        guard delta > view.bounds.height / 3 else {
            return
        }

        // Compute the assets to start caching and to stop caching.
        let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
        let addedAssets = addedRects
                .flatMap { rect in
                    collectionView.indexPathsForElements(in: rect)
                }
                .map { indexPath in
                    result.object(at: indexPath.item)
                }
        let removedAssets = removedRects
                .flatMap { rect in
                    collectionView.indexPathsForElements(in: rect)
                }
                .map { indexPath in
                    result.object(at: indexPath.item)
                }

        // Update the assets the PHCachingImageManager is caching.
        manager.startCachingImages(for: addedAssets,
                targetSize: targetSize, contentMode: .aspectFill, options: nil)
        manager.stopCachingImages(for: removedAssets,
                targetSize: targetSize, contentMode: .aspectFill, options: nil)

        // Store the preheat rect to compare against in the future.
        previousPreheatRect = preheatRect
    }

    private func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                        width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                        width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                        width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                        width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }

    @IBAction func didTapDismiss(_ sender: Any) {
        delegate?.imagePickerDidSelectAssets(assets: [])
        navigationController?.dismiss(animated: true)
    }

    @IBAction func didTapNext(_ sender: Any) {
        delegate?.imagePickerDidSelectAssets(assets: selectedAssets)
        navigationController?.dismiss(animated: true)
    }

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
}

protocol ImagePickerViewControllerDelegate: class {
    func imagePickerDidSelectAssets(assets: [PHAsset])
}
