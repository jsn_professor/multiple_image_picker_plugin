import Flutter
import UIKit
import Photos
import iOSUtility

public class SwiftMultipleImagePickerPlugin: NSObject, FlutterPlugin, UIAdaptivePresentationControllerDelegate, ImagePickerViewControllerDelegate {
    static var methodChannel: FlutterMethodChannel!
    private var pendingResult: FlutterResult?

    public static func register(with registrar: FlutterPluginRegistrar) {
        let instance = SwiftMultipleImagePickerPlugin()
        methodChannel = FlutterMethodChannel(name: "multiple_image_picker_plugin_method_channel", binaryMessenger: registrar.messenger())
        registrar.addMethodCallDelegate(instance, channel: methodChannel)
    }

    public func detachFromEngine(for registrar: FlutterPluginRegistrar) {
        SwiftMultipleImagePickerPlugin.methodChannel = nil
        pendingResult = nil
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "pickImages":
            let navigationController = UIStoryboard(name: "Storyboard", bundle: Bundle(for: MultipleImagePickerViewController.self)).instantiateInitialViewController() as! UINavigationController
            navigationController.presentationController?.delegate = self
            let controller = navigationController.viewControllers.first as! MultipleImagePickerViewController
            controller.delegate = self
            if let maxImageCount = call.arguments as? Int {
                controller.maxImageCount = maxImageCount
            }
            pendingResult?(nil)
            pendingResult = result
            UIApplication.shared.keyWindow?.topViewController?.present(navigationController, animated: true)
        default:
            result(FlutterMethodNotImplemented)
        }
    }

    public func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        pendingResult?(nil)
        pendingResult = nil
    }

    func imagePickerDidSelectAssets(assets: [PHAsset]) {
        guard !assets.isEmpty else {
            pendingResult?(nil)
            pendingResult = nil
            return
        }
        var count = 0
        var files = [String?]()
        for asset in assets {
            let resource = PHAssetResource.assetResources(for: asset).last
            if resource?.uniformTypeIdentifier == "public.heic" {
                PHImageManager.default().requestImageData(
                        for: asset,
                        options: nil) {
                    data, uti, orientation, info in
                    count += 1
                    if let data = data,
                       let image = CIImage(data: data),
                       let jpegData = CIContext().jpegRepresentation(of: image, colorSpace: CGColorSpaceCreateDeviceRGB()) {
                        do {
                            let url = FileManager.default.temporaryDirectory.appendingPathComponent("\(resource?.originalFilename.split(separator: ".").first ?? "").jpg")
                            if !FileManager.default.createFile(atPath: url.path, contents: jpegData, attributes: nil) {
                                try jpegData.write(to: url, options: .atomic)
                            }
                            files.append(url.path)
                        } catch let error {
                            print(error.localizedDescription)
                        }
                        if count >= assets.count {
                            self.pendingResult?(files)
                            self.pendingResult = nil
                        }
                    }
                }
            } else {
                asset.requestContentEditingInput(with: nil) {
                    input, _ in
                    count += 1
                    files.append(input?.fullSizeImageURL?.path)
                    if count >= assets.count {
                        self.pendingResult?(files)
                        self.pendingResult = nil
                    }
                }
            }
        }
    }
}
