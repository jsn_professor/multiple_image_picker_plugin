import 'package:flutter/services.dart';

class MultiImagePickerPlugin {
  static const MethodChannel _methodChannel = const MethodChannel('multiple_image_picker_plugin_method_channel');

  static Future<List<String>?> pickImages(int maxSize) async {
    final list = await MultiImagePickerPlugin._methodChannel.invokeMethod<List>('pickImages', maxSize);
    return list == null ? null : List<String>.from(list);
  }
}
